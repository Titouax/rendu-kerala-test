module CsvParser
  class Importer

    #Parse a CSV file and create the resources in the choosen Model
    def initialize(file_path, model)
      @file = File.read(file_path)
      @model = model
    end

    def process
      get_model
      correct_headers?
      CSV.parse(@file, {col_sep: ";", headers: true}) do |row|
        attributes = attributes(row)
        current_row = row.to_h.values[0].split(",").map{|s|s}
        object =  @model.find_by(reference: current_row[0])
        if object.nil?
          @model.create(attributes)
        else
          object.update(attributes)
        end
      end
    end

    private

    def get_model
      begin
        @model = @model.capitalize.constantize
      rescue NameError => e
        raise CustomError::ModelName.new
      end
    end

    def correct_headers?
      current_headers_file = CSV.parse(@file, {col_sep: ";", headers: true})
      current_headers =  current_headers_file.headers[0].split(",").map { |s| s }
      original_headers_file = File.read("./app/services/files/#{@model.to_s}_structure.json")
      original_headers = JSON.parse(original_headers_file).keys
      current_headers == original_headers ? true : false
    end

    def attributes(row)
      csv_attr_file = File.read("#{Rails.root}/app/services/files/#{@model.to_s}_structure.json")
      csv_attr = JSON.parse(csv_attr_file).with_indifferent_access
      if @model.to_s.underscore == "person"
        get_person_attributes(row, csv_attr)
      else
        get_building_attributes(row, csv_attr)
      end
    end

    def get_person_attributes(row, csv_attr)
      new_row = check_person_attributes_version(row)
      {
        reference: new_row[csv_attr[:reference]],
        email: new_row[csv_attr[:email]],
        home_phone_number: new_row[csv_attr[:home_phone_number]],
        mobile_phone_number: new_row[csv_attr[:mobile_phone_number]],
        firstname: new_row[csv_attr[:firstname]],
        lastname: new_row[csv_attr[:lastname]],
        address: new_row[csv_attr[:address]]
      }
    end

    def get_building_attributes(row, csv_attr)
      new_row = check_building_attributes_version(row)
      {
        reference: new_row[csv_attr[:reference]],
        address: new_row[csv_attr[:address]],
        zip_code: new_row[csv_attr[:zip_code]],
        city: new_row[csv_attr[:city]],
        country: new_row[csv_attr[:country]],
        manager_name: new_row[csv_attr[:manager_name]]
      }
    end

    def check_person_attributes_version(row)
      current_row = row.to_h.values[0].split(",").map{|s|s}
      p = Person.find_by(reference: current_row[0])
      if p.nil?
        new_row = current_row
      else
        all_versions = p.versions.all
        all_email = []
        all_home = []
        all_mobile = []
        all_address = []
        all_versions.each do |version|
          instance = version.reify
          next if instance.nil?
          all_email << instance.email
          all_home << instance.home_phone_number
          all_mobile << instance.mobile_phone_number
          all_address << instance.address
        end
        new_row = current_row
        if all_home.include?(current_row[3])
          new_row[3] = p.home_phone_number
        end
        if all_mobile.include?(current_row[4])
          new_row[4] = p.mobile_phone_number
        end
        if all_email.include?(current_row[5])
          new_row[5] = p.email
        end
        if all_address.include?(current_row[6])
          new_row[6] = p.address
        end
      end
      new_row
    end

    def check_building_attributes_version(row)
      current_row = row.to_h.values[0].split(",").map{|s|s}
      b = Building.find_by(reference: current_row[0])
      if b.nil?
        new_row = current_row
      else
        all_versions = b.versions.all
        all_manager_name = []
        all_versions.each do |version|
          instance = version.reify
          next if instance.nil?
          all_manager_name << instance.manager_name
        end
        new_row = current_row
        if all_manager_name.include?(current_row[5])
          new_row[5] = b.manager_name
        end
      end
      new_row
    end

  end
end
