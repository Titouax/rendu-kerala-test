module CustomError
  class ModelName < StandardError
    attr_reader :data
    def initialize(msg="Not support by CsvParser::Importer, try 'person' or 'building'", data="")
      @data = data
      super(msg)
    end
  end
end
